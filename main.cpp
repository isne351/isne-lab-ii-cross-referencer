#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "tree.h"
using namespace std;

int main()
{

	Tree<string> mytree;
	string output;
	vector<string> line;
	fstream sfile;

	int selected_line,pointer_line = 0;

	sfile.open("test.txt");

	if (!sfile.eof()) {
		while (getline(sfile, output)) {
			line.push_back(output);
			pointer_line++;
			istringstream split(output);
			while (split) {
				string word;
				split >> word;
				if (word == "") continue;
				mytree.insert(word,pointer_line);
			}
		}
	}
	cout << "Word : Line" << endl;
	cout << "----------" << endl;

	sfile.close();
	mytree.inorder();

	cout << endl << "Enter the line number : ";
	cin >> selected_line;

	if (selected_line <= 0 || selected_line > line.size()) {
		cout << endl << "Not found !" << endl;
	} else if (selected_line == -1) {
		return 0;
	} else {
		cout << endl << line.at(selected_line - 1) << endl;
	}
	system("pause");

}